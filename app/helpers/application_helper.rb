module ApplicationHelper
  @@base_title = 'D & G Moments'

  # Generate page title for each web page:
  def generate_full_title(page_title='')
    if page_title.empty?
      "#{@@base_title}"
    else
      "#{page_title} | #{@@base_title}"
    end
  end
end
